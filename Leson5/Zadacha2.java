package Leson5;

public class Zadacha2 {
    public static void main(String[] args) {
        double max = 0;
        double y = 0;
        for (double x = -15; x <= 15; x += 0.01) {
            if (Math.pow(x, 2) - x + 3 > Math.pow(max, 2) - max + 3) {
                max = x;
                y = Math.pow(x, 2) - x + 3;

            }

        }
        System.out.println("Значение y=" + y + " Значение x=" + max);

    }
}
