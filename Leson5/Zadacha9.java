package Leson5;

public class Zadacha9 {
    public static void main(String[] args) {
        double max = -100;
        double y = 0;
        for (double x = -100; x <= 100; x += 0.01) {
            if (Math.pow(Math.E, Math.sin(x)) > Math.pow(Math.E, Math.sin(max))) {
                max = x;
                y = Math.pow(Math.E, Math.sin(max));

            }

        }
        System.out.println("Значение y=" + y + " Значение x=" + max);
    }
}
