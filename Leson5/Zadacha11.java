package Leson5;

public class Zadacha11 {
    public static void main(String[] args) {
        double max = 0;
        double y = 0;
        for (double x = -5; x <= 5; x += 0.01) {
            if (Math.pow(x, Math.E) + x > Math.pow(max, Math.E) + x) {
                max = x;
                y = Math.pow(max, Math.E) + x;

            }

        }
        System.out.println("Значение y=" + y + " Значение x=" + max);
    }
}
