package Leson5;

public class Zadacha4 {
    public static void main(String[] args) {
        double max = 0;
        double y = 0;
        for (double x = -10; x <= 10; x += 0.01) {
            if (Math.pow(x, 2) + Math.cos(x) > Math.pow(max, 2) + Math.cos(x)) {
                max = x;
                y = Math.pow(x, 2) + Math.cos(x);

            }

        }
        System.out.println("Значение y=" + y + " Значение x=" + max);

    }
}
