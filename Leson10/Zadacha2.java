package Leson10;

public class Zadacha2 {
    public static void main(String[] args) {
        Check[] list = new Check[4];
        int kolichestvo = 0;
        double ves = 0;
        double itogo = 0;
        list[0] = new Check("огурцы", 50, 1, 4, 1.0, 4.2);
        //list[0] = new Check("огурцы", 50, 1, kolichestvo, (list[0].getCena() % list[0].getVes()), ves);
        list[1] = new Check("марковь", 47, 1.5, kolichestvo, (list[1].getCena() % list[1].getVes()), ves);
        list[2] = new Check("бананы", 90, 0.4, kolichestvo, (list[2].getCena() % list[2].getVes()), ves);
        list[3] = new Check("помидоры", 49, 2, kolichestvo, (list[3].getCena() % list[3].getVes()), ves);
        for (int i = 0; i < list.length; i++) {
            kolichestvo++;
            ves += list[i].getVes();
            itogo += list[i].getCena();
            System.out.println("Продукт: "+ list[i].getName()+" | Цена за вес: "+list[i].getCenaZaVes()+" | Вес:"+list[i].getVes()+" | Цена:"+list[i].getCena()+"||");
        }
        System.out.println(" | Количество всех продуктов: " + kolichestvo + " Вес всех продуктов: " + ves + " Итого: " + itogo + " ||");
    }
}
class Product {
    private String Name;
    private double Cena;
    private double Ves;

    public Product(String name, double cena, double ves) {
        Name = name;
        Cena = cena;
        Ves = ves;
    }

    public String getName() {
        return Name;
    }

    public double getCena() {
        return Cena;
    }

    public double getVes() {
        return Ves;
    }

}
class Buy extends Product {
    private int Colichestvo;
    private Double CenaZaVes;
    private Double ObshiyVes;

    public Buy(String name, double cena, double ves, int colichestvo, Double cenaZaVes, Double obshiyVes) {
        super(name, cena, ves);
        Colichestvo = colichestvo;
        CenaZaVes = cenaZaVes;
        ObshiyVes = obshiyVes;
    }


    public int getColichestvo() {
        return Colichestvo;
    }

    public Double getCenaZaVes() {
        return CenaZaVes;
    }

    public Double getObshiyVes() {
        return ObshiyVes;
    }

}
class Check extends Buy {
    public Check(String name, double cena, double ves, int colichestvo, Double cenaZaVes, Double obshiyVes) {
        super(name, cena, ves, colichestvo, cenaZaVes, obshiyVes);
    }

    public void printChek(){

    }
}