package Leson13;

import java.util.Random;

public class Zadacha {
    public static void main(String[] args) {
        Elf[] Elf1 = new Elf[3];
        Elf[] Elf2 = new Elf[3];
        Elf[] Elf3 = new Elf[3];
        zapolnenieElf(Elf1);
        zapolnenieElf(Elf2);
        zapolnenieElf(Elf3);


        ElfWarrior[] elfWarriors = new ElfWarrior[5];
        Integer[] integer = new Integer[5];
        Boolean[] booleans = new Boolean[5];
        Double[] doubles = new Double[5];
        Character[] characters = new Character[5];
        for (
                int i = 0;
                i < elfWarriors.length; i++) {
            elfWarriors[i] = new ElfWarrior(
                    i + 5, i * 3, i * 2, "war" + i);
            integer[i] = i;
            booleans[i] = false;
            doubles[i] = (double) i / 2;
            characters[i] = (char) (70 + i);

        }


        print(elfWarriors);
        System.out.println();

        print(integer);
        System.out.println();

        print(booleans);
        System.out.println();

        print(doubles);
        System.out.println();

        print(characters);

    }

    public static void zapolnenieElf(Elf[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            int number = random.nextInt(3);
            int numberArcher = random.nextInt(10) + 90;
            int numberWarrior = random.nextInt(10) + 60;
            int numberDruid = random.nextInt(10) + 120;
            if (number == 0) {
                array[i] = new ElfWarrior(450, 50, numberArcher, "");
            }
            if (number == 1) {
                array[i] = new ElfWarrior(600, 30, numberWarrior, "");
            }
            if (number == 2) {
                array[i] = new ElfWarrior(350, 35, numberDruid, "");
            }
        }
    }

    public static <T> void print(T[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i].toString());
        }
    }

}

abstract class Elf {
    private int hp;
    private int damage;
    private int speed;
    private String name;

    public Elf(int hp, int damage, int speed, String name) {
        this.hp = hp;
        this.damage = damage;
        this.speed = speed;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int getHp() {
        return hp;
    }

    public int getDamage() {
        return damage;
    }

    public int getSpeed() {
        return speed;
    }

    public String getName() {
        return name;
    }

    abstract void say();
}

class ElfWarrior extends Elf {

    public ElfWarrior(int hp, int damage, int speed, String name) {
        super(hp, damage, speed, name);
    }

    @Override
    public String toString() {
        return "It's warrior with name = " + super.getName() +
                " damage = " + super.getDamage() +
                " hp = " + super.getHp() +
                " speed = " + super.getSpeed();
    }

    @Override
    void say() {

    }
}