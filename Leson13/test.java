package Leson13;

public class test {
    public static void main(String[] args) {
        Mersedes mersedes = new Mersedes();
        Volvo volvo = new Volvo();

        Car[] cars = new Car[2];
        cars[0] = new Volvo();
        cars[1] = new Mersedes();
        for (int i = 0; i < cars.length; i++) {
            cars[i].go();
        }
    }
}

abstract class Car{
    String color;
    int speed;
    abstract void go();
}

class Mersedes extends Car{

    @Override
    void go() {
        System.out.println("================>");
    }
}

class Volvo extends Car{

    @Override
    void go() {
        System.out.println("------->");
    }
}
