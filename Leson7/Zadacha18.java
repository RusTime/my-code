package Leson7;

import java.util.Scanner;

public class Zadacha18 {
    public static void main(String[] args) {
        int[] data = {1, 5, 3, 7, -10, -1};
        int a = 0;

        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        int num = in.nextInt();

        for (int i = 0; i < data.length; i++) {
            if (data[i] == num) {
                a++;
            }
        }
        System.out.println("\n" + "\t" + "= " + a);
    }
}
