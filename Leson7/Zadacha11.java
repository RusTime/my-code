package Leson7;

import java.io.PrintStream;

public class Zadacha11 {
    public static void main(String[] args) {
        int[] data = {1, 5, 3, 7, -10, -1};

        for (int i = 0; i < data.length; i++)
            System.out.print(data[i] + "  ");

        for (int i = 0; i < data.length / 2; i++) {
            int k = data[i];
            data[i] = data[data.length - 1 - i];
            data[data.length - 1 - i] = k;
        }
        System.out.println();
        for (
                int i = 0;
                i < data.length; i++) {
            System.out.print(data[i] + "  ");
        }
    }
}
