package Leson14;

public class Zadacha3 {
    public static void main(String[] args) {
        funct(2, 0.2);
    }

    public static <T> void funct(T param, T marap) {
        if (param instanceof Double || marap instanceof Double) {
            System.out.println(param.getClass().getSimpleName());
            System.out.println((Double) marap + (Double) param);
        } else if (param instanceof Integer || marap instanceof Integer) {
            System.out.println(param.getClass().getSimpleName());
            System.out.println(param);
            System.out.println((Integer) param + (Integer) marap);
        } else {
            Double tmp = (Double) param + (Double) marap;
            System.out.println(tmp);
        }

    }
}
