package Leson14;

public class Zadacha4 {
    public static void main(String[] args) {
        funct("hjk");
    }

    public static <T> void funct(T param) {
        if (param instanceof Character) {
            System.out.println(param.getClass().getSimpleName());
            Character tmp = (Character) param;
            System.out.println((int) tmp);
        }
        if (param instanceof String) {
            System.out.println(param.getClass().getSimpleName());
            char[] tmp = ((String) param).toCharArray();
            for (int i = 0; i < tmp.length; i++) {
                System.out.print((int)tmp[i]+",");
            }
        }
    }
}
