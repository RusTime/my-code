package Leson12;

import java.util.Scanner;

public class Zadacha1 {
    public static void main(String[] args) {
        DateOfMeet[] dateOfMeets = new DateOfMeet[5];

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < dateOfMeets.length; i++) {
            dateOfMeets[i] = new DateOfMeet();
            System.out.println("It's " + i);
            System.out.println("Enter date");
            dateOfMeets[i].setDate(scanner.nextInt());
            scanner.reset();
            System.out.println("Enter month");
            dateOfMeets[i].setMonth(
                    Months.valueOf(scanner.next().toUpperCase())
            );
            scanner.reset();
            System.out.println("Enter day of week");
            dateOfMeets[i].setDay(
                    DayOfWeek.valueOf(scanner.next().toUpperCase())
            );
            scanner.reset();
            System.out.println("Enter name");
            dateOfMeets[i].setNameOfMeeting(scanner.next());
            scanner.reset();
            System.out.println("Enter description");
            dateOfMeets[i].setDescription(scanner.next());
            scanner.reset();
            System.out.println("Enter hours");
            dateOfMeets[i].setHours(scanner.nextInt());
            scanner.reset();
            System.out.println("Enter min");
            dateOfMeets[i].setMin(scanner.nextInt());
            scanner.reset();
            dateOfMeets[i].show();
        }
    }
}

class DateOfMeet {
    private DayOfWeek day;
    private Months month;
    private String nameOfMeeting;
    private String description;
    private int hours;
    private int min;
    private int date;

    public DateOfMeet(DayOfWeek day, Months month, String nameOfMeeting, String description, int hours, int min, int date) {
        this.day = day;
        this.month = month;
        this.nameOfMeeting = nameOfMeeting;
        this.description = description;
        this.hours = hours;
        this.min = min;
        this.date = date;
    }

    public DateOfMeet(){
        day = null;
        month = null;
        nameOfMeeting = "";
        description="";
        hours = 0;
        min = 0;
        date = 0;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public void setMonth(Months month) {
        this.month = month;
    }

    public void setNameOfMeeting(String nameOfMeeting) {
        this.nameOfMeeting = nameOfMeeting;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void show() {
        System.out.println(
                date + " "
                        + month + " " +
                        day + " - "
                        + hours + ":" + min);
        System.out.println(nameOfMeeting);
        System.out.println(description);
    }
}

enum DayOfWeek {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;
}

enum Months {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;
}