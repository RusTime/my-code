package Leson9;

public class Zadacha2 {
    public static void main(String[] args) {
        Fraction f1 = new Fraction();
        Fraction f2 = new Fraction();
        FractionLogic fl = new FractionLogic();
        Fraction f3;

        f3 = fl.multiply(f1, f2);
        f3.print();
        System.out.println();
        f3 = fl.drivide(f1, f2);
        f3.print();
        System.out.println();
        f3 = fl.plus(f1, f2);
        f3.print();
        System.out.println();
        f3 = fl.minus(f1, f2);
        f3.print();
        System.out.println();


    }
}

class Fraction {
    private int x1;
    private int x2;

    public void print() {
        System.out.println(x1);
        System.out.println("--");
        System.out.println(x2);

    }

    public Fraction() {
        this.x1 = x1;
        this.x2 = x2;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

}

class FractionLogic {
    public Fraction plus(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX2()
                + second.getX1() * first.getX2());
        res.setX2(first.getX2() * second.getX2());
        return res;
    }

    public Fraction minus(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX2()
                - second.getX1() * first.getX2());
        res.setX2(first.getX2() * second.getX2());
        return res;
    }

    public Fraction multiply(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX1());
        res.setX2(first.getX2() * second.getX2());
        return res;
    }

    public Fraction drivide(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX2());
        res.setX2(first.getX2() * second.getX1());

        return res;
    }
}