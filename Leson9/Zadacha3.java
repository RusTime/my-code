package Leson9;

public class Zadacha3 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(8, 4, 7);
        triangle.FormylaGerona();
        triangle.printTriangle();

    }
}

class Triangle {
    int a;
    int b;
    int c;

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String Tip() {
        String res = "";
        if ((c * c) == (a * a) + (b * b)) {
            res = "прямоугольным";
        } else if ((c * c) < (a * a) + (b * b)) {
            res = "остроугольным";
        } else if ((c * c) > (a * a) + (b * b)) {
            res = "тупоугольным";
        }
        return res;
    }

    public String FormylaGerona() {
        String res = "";
        double p = (a + b + c) / 2;
        return res += Math.sqrt(p * (p - a) * (p - b) * (p - c));

    }

    public void printTriangle(){
        System.out.println("Площад треугольника по формуле Герона = " + FormylaGerona() );
        System.out.println(" Треугольник является " + Tip());
    }

}