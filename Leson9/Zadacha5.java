package Leson9;

public class Zadacha5 {
    public static void main(String[] args) {
        Monster5 Org = new Monster5("Org",245, 56, 7 );
        Org.printMonster5();
    }
}

class Monster5 {
    String name;
    int HP;
    int AttackPower;
    int defense;

    public Monster5(String name, int HP, int attackPower, int defense) {
        this.name = name;
        this.HP = HP;
        AttackPower = attackPower;
        this.defense = defense;
    }

    public void printMonster5() {
        System.out.println(name + " | HP: " + HP + " | Damage: " + AttackPower + " | Defense: " + defense);
    }
}