package Leson9;

public class Zadacha4 {
    public static void main(String[] args) {
        Human4 iivan = new Human4("ivan", "ivanov", 25, true);
        iivan.printHuman4();
    }
}

class Human4 {
    String Imya;
    String Familiya;
    int vozrast;
    boolean pol;

    public Human4(String imya, String familiya, int vozrast, boolean pol) {
        Imya = imya;
        Familiya = familiya;
        this.vozrast = vozrast;
        this.pol = pol;
    }


    public void printHuman4() {
        System.out.print("Имя: " + Imya + " | Фамилия: " + Familiya + " | Возраст: " + vozrast + " | Пол: ");
        if (pol == true) {
            System.out.println("муж.");
        } else {
            System.out.println("жен.");
        }
    }

}