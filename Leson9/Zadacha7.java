package Leson9;

public class Zadacha7 {
    public static void main(String[] args) {
        Figure figure1 = new Figure(5,0,0);
        figure1.printCheck();
        Figure figure2 = new Figure(5,0,5);
        figure2.printCheck();
        Figure figure3 = new Figure(5,5,5);
        figure3.printCheck();
    }
}

class Figure {
    int x;
    int y;
    int z;

    public Figure(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String check(){
        String res="";
        if (y == 0 && z ==0){
            res = "прямая";
        }
        else if (y == 0){
            res ="прямоугольник";
        }
        else {
            res = "параллелепипед";
        }
        return res;
    }

    public void printCheck(){
        System.out.println(check());
    }

}