package Leson23;

import java.util.Comparator;
import java.util.TreeSet;

public class Zadacha1 {
    public static void main(String[] args) {
        Comparator<Warriar> wrcomp = new Warr();
        TreeSet<Warriar> warriar = new TreeSet(wrcomp);
        warriar.add(new Warriar("1", 10, 5));
        warriar.add(new Warriar("2", 10, 7));
        warriar.add(new Warriar("3", 10, 50));
        warriar.add(new Warriar("4", 10, 4));
        warriar.add(new Warriar("5", 10, 5));
        warriar.add(new Warriar("Krip", 10, 0));
        warriar.add(new Warriar("6", 10, 10));
        warriar.add(new Warriar("Gerald", 10, 99));
        warriar.add(new Warriar("7", 10, 10));
        warriar.add(new Warriar("8", 10, 6));
        for (Warriar warriar1 : warriar) {
            System.out.println(warriar1.name);
        }

    }
}


class Warriar{
    String name;
    private Integer hp;
    private Integer dm;

    public Warriar(String name, Integer hp, Integer dm) {
        this.name = name;
        this.hp = hp;
        this.dm = dm;
    }

    public Integer getDm() {
        return dm;
    }
}

class Warr implements Comparator<Warriar> {
    @Override
    public int compare(Warriar o1, Warriar o2) {
        if (o1.getDm() > o2.getDm()) {
            return 1;
        } else if (o1.getDm() < o2.getDm()) {
            return -1;
        }
        return 0;
    }
}
