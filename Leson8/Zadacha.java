package Leson8;

import java.util.Random;

public class Zadacha {
    public static void main(String[] args) {
        Generator generators = new Generator();

        DvaChisla chislo = new DvaChisla();
        chislo.showInfo();
        chislo.a = 12;
        chislo.b = 50;
        chislo.showInfo();
        chislo.suma();
        chislo.max();
        System.out.println("____________________________________________________________________________________________");

        Task4 task4 = new Task4();
        task4.printStudents();
        task4.sorterStudent();
        System.out.println("_____________________________");
        task4.printStudents();
        System.out.println("____________________________________________________________________________________________");

        HomeBiblioteka LiniaOgny = new HomeBiblioteka("Линия огня", "Василий Орехов",2008, 645);
        LiniaOgny.printBiblioteka();
        HomeBiblioteka ZonaPorojeniy = new HomeBiblioteka("Зона поражения", "Василий Орехов",2008, 750);
        ZonaPorojeniy.printBiblioteka();
        HomeBiblioteka ViborOrujiy = new HomeBiblioteka("Выбор оружия","Андрей Левицкий",2008 ,697);
        ViborOrujiy.printBiblioteka();
        HomeBiblioteka PustieZemli = new HomeBiblioteka("Пустые земли","Алексей Калугин",2009 ,535);
        PustieZemli.printBiblioteka();
        HomeBiblioteka ZakonProklytogo = new HomeBiblioteka("Закон проклятого","Дмитрий Силлов",2012 ,752);
        ZakonProklytogo.printBiblioteka();
        HomeBiblioteka KromeshniySvet = new HomeBiblioteka("Новая Зона. Кромешный свет","Сергей Слюсаренко",2014 ,526);
        KromeshniySvet.printBiblioteka();





    }
}

//zadacha1
class Human {
    String Name;
    String Surname;
    int age;
    int gender;
}

//zadach2
class Monster {
    String Name;
    int hp;
    int attackPower;
    int defense;
}

//zadacha3
class DvaChisla {
    int a;
    int b;

    void showInfo() {
        System.out.println(a + " : " + b);
    }

    void suma() {
        System.out.println(a + b);
    }

    void max() {
        if (a > b) {
            System.out.println(a);
        } else {
            System.out.println(b);
        }
    }


}

//zadacha4
class student {

    String Surname;
    String FIO;
    int numberGroups;
    int[] progress;

    public student(String surname, String FIO, int numberGroups, int[] progress) {
        Surname = surname;
        this.FIO = FIO;
        this.numberGroups = numberGroups;
        this.progress = progress;
    }

    public double getAverage() {
        double res = 0;
        for (int i = 0; i < progress.length; i++) {
            res += progress[i];
        }
        res = res / progress.length;
        return res;
    }

    public void printStudent() {
        System.out.println("_|_"+Surname+"|"+FIO+"|"+numberGroups+"|"+getAverage());
    }
}
class Generator {
    String[] partsOfName = {"na", "ma", "no", "bo", "hi", "lu", "o", "a", "pe", "i", "y", "hu", "y", "ch", "ka", "sh"};

    public String Names() {
        Random random = new Random();
        String res = "";
        res += (char) (random.nextInt(27) + 65);
        for (int i = 0; i < random.nextInt(5) + 1; i++) {
            res += partsOfName[random.nextInt(partsOfName.length)];

        }
        return res;
    }
}
class Task4 {
    student[] students;

    Task4() {
        Generator purpleGenerator = new Generator();
        Random random = new Random();
        students = new student[10];
        for (int i = 0; i < students.length; i++) {
            students[i] = new student(
                    purpleGenerator.Names(),
                    purpleGenerator.Names(),
                    random.nextInt(3),
                    new int[]{
                            random.nextInt(5) + 1,
                            random.nextInt(5) + 1,
                            random.nextInt(5) + 1,
                            random.nextInt(5) + 1,
                            random.nextInt(5) + 1,}
            );
        }
    }

    public void sorterStudent() {
        student tmp;
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length-1; j++) {
                if (students[j].getAverage() >
                        students[j + 1].getAverage()) {
                    tmp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = tmp;
                }
            }
        }
    }

    public void printStudents() {
        for (int i = 0; i < students.length; i++) {
            students[i].printStudent();
        }

    }
}

//zadacha5
class HomeBiblioteka {
    String NameBook;
    String Avtor;
    int GodVipuska;
    int stroniza;

    public HomeBiblioteka(String nameBook, String avtor, int godVipuska, int stroniza) {
        NameBook = nameBook;
        Avtor = avtor;
        this.GodVipuska = godVipuska;
        this.stroniza = stroniza;
    }

    public void printBiblioteka(){
        System.out.println(NameBook+"|"+Avtor+"|"+GodVipuska+"|"+stroniza+"|");
    }

    public void  poiskKnigi(){

    }

}
