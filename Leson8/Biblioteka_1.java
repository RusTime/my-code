package Leson8;

import java.util.Random;

public class Biblioteka_1 {
    public static void main(String[] args) {
        Lib lib = new Lib(2);
        Book book = new Book("Линия огня", "Василий Орехов", 2008, 645);
        lib.printLib();
        System.out.println();
        lib.add(book);
        lib.printLib();
        System.out.println();
        lib.add(new Book("Зона поражения", "Василий Орехов", 2008, 750));
        lib.printLib();
        System.out.println();
        lib.sortByYears();
        lib.printLib();
        lib.sortStroniza();
        lib.printLib();
    }
}

class Lib {
    Book[] books;
    int size;

    Lib(int size) {
        books = new Book[size];
        this.size = size;
        for (int i = 0; i < books.length; i++) {
            books[i] = new Book();

        }
    }

    public void printLib() {
        for (int i = 0; i < size; i++) {
            books[i].printBook();
        }
    }

//    public void sortNameBook() {
//        for (int i = 0; i < size; i++) {
//            for (int j = 0; j < size - 1; j++) {
//                if (books[j].NameBook > books[j + 1].NameBook) {
//                    Book tmp = books[j];
//                    books[j] = books[j + 1];
//                    books[j + 1] = tmp;
//                }
//            }
//        }
//    }
//
//    public void sortAvtor() {
//        for (int i = 0; i < size; i++) {
//            for (int j = 0; j < size - 1; j++) {
//                if (books[j].Avtor > books[j + 1].Avtor) {
//                    Book tmp = books[j];
//                    books[j] = books[j + 1];
//                    books[j + 1] = tmp;
//                }
//            }
//        }
//    }

    public void sortByYears() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - 1; j++) {
                if (books[j].GodVipuska > books[j + 1].GodVipuska) {
                    Book tmp = books[j];
                    books[j] = books[j + 1];
                    books[j + 1] = tmp;
                }
            }
        }
    }

    public void sortStroniza() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - 1; j++) {
                if (books[j].stroniza > books[j + 1].stroniza) {
                    Book tmp = books[j];
                    books[j] = books[j + 1];
                    books[j + 1] = tmp;
                }
            }
        }
    }

    public void add(Book book) {
        if (books.length == size) {
            Book[] tmp = new Book[books.length * 2];
            for (int i = 0; i < size; i++) {
                tmp[i] = books[i];
            }
            books = tmp;
        }
        books[size] = book;
        size++;
    }
}

class Book {
    String NameBook;
    String Avtor;
    int GodVipuska;
    int stroniza;

    public Book(String nameBook, String avtor, int godVipuska, int stroniza) {
        NameBook = nameBook;
        Avtor = avtor;
        GodVipuska = godVipuska;
        this.stroniza = stroniza;
    }

    public Book() {
        NameGenerator generator = new NameGenerator();
        Random random = new Random();
        NameBook = "-" + generator.generateNames() + "-";
        Avtor = generator.generateNames();
        GodVipuska = random.nextInt(200) + 1800;
        stroniza = random.nextInt(200) + 500;
    }

    public void printBook() {
        System.out.println(NameBook + " " + Avtor + " " + GodVipuska + " " + stroniza);
    }
}

class NameGenerator {
    String[] partsOfNames = {"na", "ma", "no", "bo", "i", "y", "e",
            "hi", "lu", "o", "a", "pe", "lol", "kek", "ra",
            "cheb", "oma", "hu"};

    public String generateNames() {
        Random random = new Random();
        String res = "";
        res += (char) (random.nextInt(27) + 65);
        for (int i = 0; i < random.nextInt(5) + 1; i++) {

            res += partsOfNames[random.nextInt(partsOfNames.length)];
        }
        return res;
    }
}
