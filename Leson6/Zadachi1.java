package Leson6;

import java.util.Random;

public class Zadachi1 {
    public static void main(String[] args) {
       int[][] data = {{9,2,-1},{-1,3,10},{4,-7,5}};//new int[3][3]
//        fillRandom(data, 10);
        System.out.println("Before");
        printArray(data);
        sort2dArray(data);
        System.out.println("After");
        printArray(data);
    }


    static void sort2dArray(int[][] array) {
        //System.out.println("сортируем двумерный массив");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (summElemArray(array[j]) < summElemArray(array[j + 1])) {
                    //меняем местами строки в массиве
                    int[] tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }


    static int summElemArray(int[] array) {
        //System.out.println("сумма элементов в одномерном массиве");
        int res = 0;
        for (int i = 0; i < array.length; i++) {
            res += array[i];
        }
        return res;
    }


    static void printArray(int[][] array) {
        //System.out.println("печать двумерного массива");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.print(" = " + summElemArray(array[i])+"\n"  );
        }
    }


//    static void fillRandom(int[][] array,int bound) {
//        //System.out.println("заполняем двумерный массив случайными значениями до заданного диапазона");
//        Random random = new Random();
//        for (int i = 0; i < array.length; i++) {
//            for (int j = 0; j < array[i].length; j++) {
//                array[i][j] = random.nextInt(bound) + 10;
//            }
//        }
//    }
}