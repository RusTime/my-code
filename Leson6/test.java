package Leson6;

public class test {
    public static void main(String[] args) {
        int x = (int) (Math.random() * 8 + 3);
        int[][] arrayTest = new int[x][];
        for (int i = 0; i < x; ++i) {
            arrayTest[i] = new int[(int) (Math.random() * 10 + 1)];
        }
        System.out.println("Исходный массив: ");
        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < arrayTest[i].length; ++j) {
                arrayTest[i][j] = (int) (Math.random() * 50 - 5);
                System.out.print(arrayTest[i][j] + "\t");
            }
            System.out.println();
        }
        int temp = 0;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < arrayTest[i].length; j++) {
                for (int k = 0; k < x; k++) {
                    for (int n = 0; n < arrayTest[k].length; n++) {
                        if (arrayTest[k][n] > arrayTest[i][j]) {
                            temp = arrayTest[k][n];
                            arrayTest[k][n] = arrayTest[i][j];
                            arrayTest[i][j] = temp;
                        }
                    }
                }
            }
        }
        System.out.println("\nУпорядоченный массив по возрастанию : ");
        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < arrayTest[i].length; ++j) {
                System.out.print(arrayTest[i][j] + "\t");
            }
            System.out.println();
        }
    }

}


