package Leson6;

public class zadacha001 {
    public static void main(String[] args) {
        int[][] data =  {{9, 2, 1}, {5, 3, -10}, {7, 4, 10}};
        int[][] data1 = {{9, 2, 1}, {5, 3, -10}, {7, 4, 10}};
        int[][] data2 = {{9, 2, 1}, {5, 3, -10}, {7, 4, 10}};

        int[] tmp = new int[data.length * data[0].length];//тут будет двумерный массив когда перенесем его в одномерный
        int countOfTMP = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                tmp[countOfTMP] = data[i][j];
                countOfTMP++;
            }
        }

        printData(data);
        summElemStrok(tmp);
        printKolon(data);
        SortVozr(data);
        SortUb(data);
        SortStolbUb(data1);
        SortStolbVozr(data1);
        SortKolonUb(data2);
        SortKolonVozr(data2);


    }
    static void printKolon(int[][] array){
        System.out.println("Сумма строк и колонн массива : ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.print(" = " + summElemStrok(array[i]) + "\n");
        }
        for (int i = 0; i < array[0].length; i++) {
            System.out.print("||" + "  ");
        }
        System.out.println();
        for (int i = 0; i < array[0].length; i++) {
            System.out.print(summElemKolon(array, i) + "   ");
        }
    }

    static void printData(int[][] array) {
        System.out.println("\nНачальный массив : ");
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; ++j) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void changeKolon(int[][] array, int x1, int x2) {
        int tmp = 0;
        for (int i = 0; i < array.length; i++) {
            tmp = array[i][x1];
            array[i][x1] = array[i][x2];
            array[i][x2] = tmp;
        }
    }

    static int summElemStrok(int[] array) {
        //сумма элементов в одномерном массиве
        int res = 0;
        for (int i = 0; i < array.length; i++) {
            res += array[i];
        }
        return res;
    }

    static int summElemKolon(int[][] array, int num) {
        //сумма элементов колонн массива
        int summ = 0;
        for (int i = 0; i < array.length; i++) {
            summ += array[i][num];
        }
        return summ;
    }

    static void SortVozr(int[][] array) {
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array.length; k++) {
                    for (int n = 0; n < array[k].length; n++) {
                        if (array[k][n] > array[i][j]) {
                            temp = array[k][n];
                            array[k][n] = array[i][j];
                            array[i][j] = temp;
                        }
                    }
                }
            }
        }
        System.out.println("\nОтсортированный массив по возрастанию : ");
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; ++j) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void SortUb(int[][] array) {
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array.length; k++) {
                    for (int n = 0; n < array[k].length; n++) {
                        if (array[k][n] < array[i][j]) {
                            temp = array[k][n];
                            array[k][n] = array[i][j];
                            array[i][j] = temp;
                        }
                    }
                }
            }
        }
        System.out.println("\nОтсортированный массив по убыванию : ");
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; ++j) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }


    }

    static void SortStolbUb(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (summElemStrok(array[j]) < summElemStrok(array[j + 1])) {
                    int[] tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        System.out.println("\nОтсортированные строки в массиве по убыванию : ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.print(" = " + summElemStrok(array[i]) + "\n");
        }

    }

    static void SortStolbVozr(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (summElemStrok(array[j]) > summElemStrok(array[j + 1])) {
                    int[] tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        System.out.println("\nОтсортированные строки в массиве по возростанию : ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.print(" = " + summElemStrok(array[i]) + "\n");
        }

    }

    static void SortKolonUb(int[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                if (summElemKolon(array, j) < summElemKolon(array, j + 1)) {
                    changeKolon(array, j, j + 1);
                }
            }
        }
        System.out.println("\nОтсортированные колоны в массиве по убыванию : ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        for (int i = 0; i < array[0].length; i++) {
            System.out.print("||" + "  ");
        }
        System.out.println();
        for (int i = 0; i < array[0].length; i++) {
            System.out.print(summElemKolon(array, i) + "   ");
        }
    }

    static void SortKolonVozr(int[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                if (summElemKolon(array, j) > summElemKolon(array, j + 1)) {
                    changeKolon(array, j, j + 1);
                }
            }
        }
        System.out.println("\nОтсортированные колоны в массиве по возростанию : ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        for (int i = 0; i < array[0].length; i++) {
            System.out.print("||" + "  ");
        }
        System.out.println();
        for (int i = 0; i < array[0].length; i++) {
            System.out.print(summElemKolon(array, i) + "   ");
        }
    }

}
