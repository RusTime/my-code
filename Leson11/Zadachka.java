package Leson11;

import java.util.Random;
import java.util.Scanner;

public class Zadachka {

    public static void main(String[] args) {
        printNew();
    }

    public static void printNew() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Привет. Напиши 'newstart' чтобы начать бой. Hапиши 'again' чтобы повторить или 'end' чтобы закончить: ");
        Active active = Active.valueOf(scanner.next());
        System.out.println("------------------------------------------------------------------------------------------------------");
        System.out.print("Напиши число войнов в команде: ");
        int numberTeam = scanner.nextInt();
        System.out.println("-----------------------------------");
        Warrior[] TeamA = new Warrior[numberTeam];
        Warrior[] TeamB = new Warrior[numberTeam];
        Warrior[] TeamA1=null;
        Warrior[] TeamB1=null;
        while (active != Active.end) {
            if (active == Active.newstart) {
                zapolnenieTeam(TeamA);
                TeamA1 = copyOfWarrior(TeamA);
                zapolnenieTeam(TeamB);
                TeamB1 = copyOfWarrior(TeamB);
                proverkaAB(TeamA, TeamB);
            }
            if (active == Active.again) {
                TeamA = copyOfWarrior(TeamA1);
                TeamB = copyOfWarrior(TeamB1);
                proverkaAB(TeamA, TeamB);
            }
            System.out.print("Напишите команду('newstart','again','end'): ");
            active = Active.valueOf(scanner.next());
            System.out.println();
        }
    }

    public static Warrior[] copyOfWarrior(Warrior[] warriors) {
        Warrior[] res = new Warrior[warriors.length];
        for (int i = 0; i < warriors.length; i++) {
            res[i] = warriors[i].getCopy();
        }
        return res;
    }

    public static void proverkaAB(Warrior[] array, Warrior[] array2) {
        printTeam(array, array2);
        vstrecha(array, array2);
        int A = 0;
        int B = 0;
        while (A < array.length - 1 && B < array2.length - 1) {
            A = 0;
            B = 0;
            System.out.println();
            System.out.println("   Их ещё много, давай ещё раз!");
            System.out.println();
            System.out.println("------------next_round------------");
            System.out.println();
            printTeam(array, array2);
            vstrecha(array, array2);
            for (int j = 0; j < array.length; j++) {
                if (array[j].status == Status.Dead) {
                    A += 1;
                }
                if (array2[j].status == Status.Dead) {
                    B += 1;
                }
            }
            System.out.println("\n");
            System.out.println("Мертвых в первой команде : " + A + " || Мертвых во второй команде : " + B);
        }
        if (A == array.length - 1) System.out.println("Победила вторая команда!");
        if (B == array2.length - 1) System.out.println("Победила первая команда!");
        System.out.println("\n");
    }

    public static void vstrecha(Warrior[] array, Warrior[] array2) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].status == Status.Alive && array2[i].status == Status.Alive) {
                if (array[i].hp < (array2[i].damag - array[i].def)) {
                    array[i].FullDef();
                }
                if (array2[i].hp < (array[i].damag - array2[i].def)) {
                    array2[i].FullDef();
                }
                if (array[i].hp > 0) {
                    array[i].hp -= (array2[i].damag - array[i].def);
                }
                if (array2[i].hp > 0) {
                    array2[i].hp -= (array[i].damag - array2[i].def);
                }
            }
        }
    }

    public static void zapolnenieTeam(Warrior[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            int number = random.nextInt(3);
            int numberKintght = random.nextInt(10) + 60;
            int numberArcher = random.nextInt(10) + 90;
            int numberMagician = random.nextInt(10) + 120;
            if (number == 0) {
                array[i] = new Knight(600, 50, numberKintght, "Knight", Status.Alive);
            }
            if (number == 1) {
                array[i] = new Archer(450, 30, numberArcher, "Archer", Status.Alive);
            }
            if (number == 2) {
                array[i] = new Magician(350, 20, numberMagician, "Magician", Status.Alive);
            }
        }
    }

    public static void printTeam(Warrior[] array, Warrior[] array2) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].hp < 1) {
                array[i].status = Status.Dead;
                Warrior tmp ;
                if (array[i].status == Status.Dead && i < array.length) {
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
            if (array2[i].hp < 1) {
                array2[i].status = Status.Dead;
                Warrior tmp ;
                if (array2[i].status == Status.Dead && i < array2.length) {
                    tmp = array2[i];
                    array2[i] = array2[i + 1];
                    array2[i + 1] = tmp;
                }
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i].status == Status.Alive && array2[i].status == Status.Alive) {
                System.out.print("[" + array[i].status + "] #" + (i + 1) + " |Class: " + array[i].name + " |HP: " + array[i].hp + " |Def: " + array[i].def + " |Damag: " + array[i].damag + "  |  ");
            } else {
                System.out.print("--[" + array[i].status + "] #" + (i + 1) + " (" + array[i].name + ")--  |  ");
            }
        }
        System.out.println();
        for (int i = 0; i < array2.length; i++) {
            if (array2[i].status == Status.Alive && array[i].status == Status.Alive) {
                System.out.print("[" + array2[i].status + "] #" + (i + 1) + " |Class: " + array2[i].name + " |HP: " + array2[i].hp + " |Def: " + array2[i].def + " |Damag: " + array2[i].damag + "  |  ");
            } else {
                System.out.print("--[" + array2[i].status + "] #" + (i + 1) + " (" + array2[i].name + ")--  |  ");
            }
        }
    }
}

abstract class Warrior {
    public String name;
    double hp;
    int def;
    int damag;
    Status status;

    abstract Warrior getCopy();

    abstract void toDamage();

    abstract void FullDef();

    public Warrior(double hp, int def, int damag, String name, Status status) {
        this.hp = hp;
        this.def = def;
        this.damag = damag;
        this.name = name;
        this.status = status;
    }
}

class Knight extends Warrior {

    public Knight(double hp, int def, int damag, String name, Status status) {
        super(hp, def, damag, name, status);
    }

    @Override
    Warrior getCopy() {
        return new Knight(hp,def,damag,name,status);
    }

    @Override
    void toDamage() {
        System.out.println("удар");
    }

    @Override
    void FullDef() {
        super.def = super.def + 10;
    }

    void FullDef2() {
        System.out.println("поднять щиты");
    }
}

class Archer extends Warrior {

    public Archer(double hp, int def, int damag, String name, Status status) {
        super(hp, def, damag, name, status);
    }

    @Override
    Warrior getCopy() {
        return new Archer(hp,def,damag,name,status);
    }

    @Override
    void toDamage() {
        System.out.println("выстрел");
    }

    @Override
    void FullDef() {
        super.def = super.def + 15;
    }

    void FullDef2() {
        System.out.println("уклонение");
    }
}

class Magician extends Warrior {

    public Magician(double hp, int def, int damag, String name, Status status) {
        super(hp, def, damag, name, status);
    }

    @Override
    Warrior getCopy() {
        return new Magician(hp,def,damag,name,status);
    }

    @Override
    void toDamage() {
        System.out.println("фаер Бол");
    }

    @Override
    void FullDef() {
        super.def = super.def * 2;

    }

    void FullDef2() {
        System.out.println("магия защиты");
    }
}

enum Status {
    Alive,
    Dead;
}

enum Active {
    newstart,
    again,
    end;
}