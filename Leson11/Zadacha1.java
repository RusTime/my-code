package Leson11;

public class Zadacha1 {
    public static void main(String[] args) {
        DomesticDog domesticDog = new DomesticDog("",true,15,"","",3);
        ServiceDog serviceDog = new ServiceDog("",true,15,"","",3);
        HomelessDog homelessDog =new HomelessDog("",true,15,"","",3);
    }

}

abstract class Dog{
    String klichka;
    boolean pol ;
    int ves;

    public Dog(String klichka, boolean pol, int ves) {
        this.klichka = klichka;
        this.pol = pol;
        this.ves = ves;
    }

    abstract void Voice();

    abstract void Action();
}

class DomesticDog extends Dog {
    String address = null;
    String organization = null;
    int area =0 ;

    public DomesticDog(String klichka, boolean pol, int ves, String address, String organization, int area) {
        super(klichka, pol, ves);
        this.address = address;
        this.organization = organization;
        this.area = area;
    }

    @Override
    void Voice() {

    }

    @Override
    void Action() {

    }
}

class ServiceDog extends Dog {
    String address = null;
    String organization = null;
    int area =0 ;

    public ServiceDog(String klichka, boolean pol, int ves, String address, String organization, int area) {
        super(klichka, pol, ves);
        this.address = address;
        this.organization = organization;
        this.area = area;
    }

    @Override
    void Voice() {

    }

    @Override
    void Action() {

    }
}

class HomelessDog extends Dog {
    String address = null;
    String organization = null;
    int area =0 ;

    public HomelessDog(String klichka, boolean pol, int ves, String address, String organization, int area) {
        super(klichka, pol, ves);
        this.address = address;
        this.organization = organization;
        this.area = area;
    }

    @Override
    void Voice() {

    }

    @Override
    void Action() {

    }
}