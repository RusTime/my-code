package Leson11;

public class Zadacha3 {

}
abstract class Orc {
    int damag;
    int def;
    int hp;
    TipAtaka tipataka;

    abstract void Attack();
}

class OrcPeon extends Orc {
    @Override
    void Attack() {
        super.hp = 200;
        super.def = 15;
        super.damag = 50;
        super.tipataka = TipAtaka.crushing;
        System.out.println("Damag = "+damag+" | Tip Attack: "+tipataka+" | ");
    }
}

class OrcGrunt extends Orc {

    @Override
    void Attack() {
        super.hp = 200;
        super.def = 15;
        super.damag = 50;
        super.tipataka = TipAtaka.hacking;
        System.out.println("Damag = "+damag+" | Tip Attack: "+tipataka+" | ");
    }
}

class OrcShaman extends Orc {
    @Override
    void Attack() {
        super.hp = 200;
        super.def = 15;
        super.damag = 50;
        super.tipataka = TipAtaka.magical;
        System.out.println("Damag = "+damag+" | Tip Attack: "+tipataka+" | ");
    }
}

enum TipAtaka {
    crushing,
    hacking,
    magical;
}