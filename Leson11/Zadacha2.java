package Leson11;

public class Zadacha2 {
}

abstract class Rodent {
    Pol pol;
    int ves;
    String cvet;
    int speed;

    abstract void Run();

    abstract void Jump();

    abstract void Eat();
}
class Hamster extends Rodent{

    @Override
    void Run() {
        super.speed = 5;
        System.out.println("Hamster speed = "+speed);
    }

    @Override
    void Jump() {

    }

    @Override
    void Eat() {

    }
}
class Chinchilla extends Rodent{

    @Override
    void Run() {
        super.speed = 7;
        System.out.println("Chinchilla speed = "+speed);
    }

    @Override
    void Jump() {

    }

    @Override
    void Eat() {

    }
}
class Rat extends Rodent{

    @Override
    void Run() {
        super.speed = 50;
        System.out.println("Rat speed = "+speed);
    }

    @Override
    void Jump() {

    }

    @Override
    void Eat() {

    }
}

enum Pol {
    Female,
    Male;
}