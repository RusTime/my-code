package Leson24;

import java.util.ArrayList;
import java.util.Random;

public class Zadacha2 {
    public static void main(String[] args) {
        ArrayList<City> Kingdom = new ArrayList<>();
        Zapolnenie(Kingdom);
        Proces(Kingdom);
    }

    public static void Proces(ArrayList<City> Kingdom) {
        int a = 5;
        while (a != 0) {
            for (int i = 0; i < Kingdom.size(); i++) {
                Kingdom.get(i).Plague();
                System.out.println(Kingdom.get(i).hp);
            }
            a--;
        }
        int x =0;
        for (int i = 0; i < Kingdom.size(); i++) {
            if (Kingdom.get(i).hp > 0){
                x++;
            }
        }
        System.out.println(x);
    }

    public static void Zapolnenie(ArrayList<City> Kingdom) {
        Kingdom.add(new King());
        for (int i = 0; i < 10; i++) {
            Kingdom.add(new Nobleman());
        }
        for (int i = 0; i < 25; i++) {
            Kingdom.add(new Knight());
        }
        for (int i = 0; i < 100; i++) {
            Kingdom.add(new Peasant());
        }
    }
}

abstract class City {
    int hp;

    public City(int hp) {
        this.hp = hp;
    }

    abstract void Plague();
}

class King extends City {

    public King() {
        super(9999);
    }

    @Override
    void Plague() {
        Random random = new Random();
        hp -= random.nextInt(10) + 5;
    }
}

class Nobleman extends City {

    public Nobleman() {
        super(160);
    }

    @Override
    void Plague() {
        Random random = new Random();
        hp -= random.nextInt(10) + 7;
    }
}

class Knight extends City {

    public Knight() {
        super(150);
    }

    @Override
    void Plague() {
        Random random = new Random();
        hp -= random.nextInt(10) + 9;
    }
}

class Peasant extends City {

    public Peasant() {
        super(50);
    }

    @Override
    void Plague() {
        Random random = new Random();
        hp -= random.nextInt(10) + 10;
    }
}