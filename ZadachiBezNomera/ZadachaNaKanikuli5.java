package ZadachiBezNomera;

public class ZadachaNaKanikuli5 {
    public static void main(String[] args) {
        int[][] data = new int[5][5];
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (i == j) {
                    data[i][j] = 3;
                } else if (i + 1 == j) {
                    data[i][j] = 2;
                } else data[i][j] = 1;
            }
        }
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(data[i][j]+" ");
            }
            System.out.println();
        }
    }
}

