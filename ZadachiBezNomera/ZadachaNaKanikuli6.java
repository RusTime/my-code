package ZadachiBezNomera;

import java.util.Random;

public class ZadachaNaKanikuli6 {
    public static void main(String[] args) {
        Random random = new Random();
        int[][] data = new int[5][5];
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (i == j) {
                    data[i][j] = random.nextInt(9)+1;
                }  else data[i][j] = 1;
            }
        }
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
    }
}
