package ZadachiBezNomera;

import java.util.Scanner;

import static jdk.nashorn.internal.objects.NativeString.toLowerCase;
import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

public class ZadachaNaKanikuli3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String[] data = text.split("");
        proces(data);

    }

    public static void proces(String[] data) {
        for (int i = 0; i < data.length; i++) {
            data[i] = toLowerCase(data[i]);
            System.out.println(data[i]);
        }
        data[0] = toUpperCase(data[0]);
        for (int i = 0; i < data.length; i++) {
            if (data[i] == " " && data[i + 1] == " ") {
                data[i] = "";
            }
            if (data[i] == " " && data[i + 1] != " ") {
                data[i + 1] = toLowerCase(data[i + 1]);
            }
        }
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i]);
        }
        System.out.println();
    }

}
