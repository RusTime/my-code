package ZadachiBezNomera;

public class ZadachaNaKanikuli4 {
    public static void main(String[] args) {
        System.out.println(max(1, 5));
        System.out.println(max(4, min(3, 6)));
        System.out.println(min(max(1, max(5, 3)), min(9, 0)));
    }

    public static int max(int x, int y) {
        int numer = 0;
        if (x > y) {
            numer = x;
        } else if (x < y) {
            numer = y;
        }
        return numer;
    }

    public static int min(int x, int y) {
        int numer = 0;
        if (x > y) {
            numer = y;
        } else if (x < y) {
            numer = x;
        }
        return numer;
    }
}
