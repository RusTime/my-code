package ZadachiBezNomera;

import java.util.Random;

public class ZadachaSOrcami {
    public static void main(String[] args) {
        ARMY army = new ARMY();
        FARM farm = new FARM();
        CASTLE castle = new CASTLE();

        zapolnenieOrc(army.ARMY);
        army.gifBonus();
        printOrc(army.ARMY);
        zapolnenieOrc(farm.FARM);
        farm.gifBonus();
        printOrc(farm.FARM);
        zapolnenieOrc(castle.CASTLE);
        castle.gifBonus();
        printOrc(castle.CASTLE);
        System.out.println("-----------------------------------------------------------------------");
        Battle(army.ARMY, farm.FARM, castle.CASTLE);
    }

    public static void Battle(Orc[] army, Orc[] farm, Orc[] castle) {
        Orc[] X = army;
        Orc[] Y = farm;
        printTeam(X, Y);
        vstrecha(X, Y);
        int A = 0;
        int B = 0;
        while (A < X.length  && B < Y.length ) {
            A = 0;
            B = 0;
            System.out.println();
            System.out.println("------------next_round------------");
            System.out.println();
            printTeam(X, Y);
            vstrecha(X, Y);
            for (int j = 0; j < X.length; j++) {
                if (X[j].STATUS == Status.Dead) {
                    A += 1;
                }
                if (Y[j].STATUS == Status.Dead) {
                    B += 1;
                }
            }
            System.out.println("\n");
            System.out.println("Мертвых в первой команде : " + A + " || Мертвых во второй команде : " + B);
        }
        if (A == X.length - 1) System.out.println("Победила вторая команда!");
        if (B == Y.length - 1) System.out.println("Победила первая команда!");
        System.out.println("\n");

    }

    public static void vstrecha(Orc[] array, Orc[] array2) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].STATUS == Status.Alive && array2[i].STATUS == Status.Alive) {
                if (array[i].HP > 0) {
                    if (array2[i].DAMAG < array[i].DEF) {
                        array[i].HP -= 1;
                    } else {
                        array[i].HP -= (array2[i].DAMAG - array[i].DEF);
                    }
                }
                if (array2[i].HP > 0) {
                    if (array[i].DAMAG < array2[i].DEF) {
                        array2[i].HP -= 1;
                    } else {
                        array2[i].HP -= (array[i].DAMAG - array2[i].DEF);
                    }
                }
            }
        }
    }

    public static void zapolnenieOrc(Orc[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            int number = random.nextInt(3);
            if (number == 0) {
                array[i] = new OrcFarmer();
            }
            if (number == 1) {
                array[i] = new OrcWarrior();
            }
            if (number == 2) {
                array[i] = new OrcMagician();
            }
            if (number == 3) {
                array[i] = new OrcCommander();
            }
        }
    }

    public static void printOrc(Orc[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].HP < 1) {
                array[i].STATUS = Status.Dead;
            }
        }
        double maxFood = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].STATUS == Status.Alive) {
                System.out.print("[" + array[i].STATUS + "] Orc.№" + (i + 1) + " |Class: " + array[i].NAME + " |HP: " + array[i].HP + " |Def: " + array[i].DEF + " |Damag: " + array[i].DAMAG + " |Food: " + array[i].EDA + "  |");
                maxFood += array[i].EDA;
            } else {
                System.out.print("--[" + array[i].STATUS + "] Orc.№" + (i + 1) + " (" + array[i].NAME + ")--  |  ");
            }
        }
        System.out.print("Total food: " + maxFood + "  |  ");
        System.out.println();
    }

    public static void printTeam(Orc[] array, Orc[] array2) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].HP < 1) {
                array[i].STATUS = Status.Dead;
            }
            if (array2[i].HP < 1) {
                array2[i].STATUS = Status.Dead;
            }
        }
        for (int i = 0; i < array.length-1; i++) {
            Orc tmp ;
            if (array[i].STATUS == Status.Dead && i < array.length) {
                tmp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = tmp;
            }
        }
        for (int i = 0; i < array2.length-1; i++) {
            Orc tmp ;
            if (array2[i].STATUS == Status.Dead && i < array2.length) {
                tmp = array2[i];
                array2[i] = array2[i + 1];
                array2[i + 1] = tmp;
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i].STATUS == Status.Alive && array2[i].STATUS == Status.Alive) {
                System.out.print("[" + array[i].STATUS + "] #" + (i + 1) + " |Class: " + array[i].NAME + " |HP: " + array[i].HP + " |Def: " + array[i].DEF + " |Damag: " + array[i].DAMAG + "  |  ");
            } else {
                System.out.print("--[" + array[i].STATUS + "] #" + (i + 1) + " (" + array[i].NAME + ")--  |  ");
            }
        }
        System.out.println();
        for (int i = 0; i < array2.length; i++) {
            if (array2[i].STATUS == Status.Alive && array[i].STATUS == Status.Alive) {
                System.out.print("[" + array2[i].STATUS + "] #" + (i + 1) + " |Class: " + array2[i].NAME + " |HP: " + array2[i].HP + " |Def: " + array2[i].DEF + " |Damag: " + array2[i].DAMAG + "  |  ");
            } else {
                System.out.print("--[" + array2[i].STATUS + "] #" + (i + 1) + " (" + array2[i].NAME + ")--  |  ");
            }
        }
    }
}

abstract class Orc {
    String NAME;
    double HP;
    double DAMAG;
    double DEF;
    double EDA;
    Status STATUS;

    public Orc(String NAME, double HP, double DAMAG, double DEF, double EDA, Status STATUS) {
        this.NAME = NAME;
        this.HP = HP;
        this.DAMAG = DAMAG;
        this.DEF = DEF;
        this.EDA = EDA;
        this.STATUS = STATUS;
    }

}

class OrcFarmer extends Orc {
       Random random = new Random();
//    int numberFarmer = random.nextInt(5);


    public OrcFarmer() {
        super("Farmer", 50, 18, 5, 30, Status.Alive);
        DEF = random.nextInt(5);
    }
}

class OrcWarrior extends Orc {
    //    int numberWarrior = random.nextInt(10) + 15;
    public OrcWarrior() {
        super("Warrior", 175, 35, 25, 15, Status.Alive);
    }
}

class OrcMagician extends Orc {
    //int numberMagician = random.nextInt(7) + 10;
    public OrcMagician() {
        super("Magician", 100, 50, 17, 10, Status.Alive);
    }
}

class OrcCommander extends Orc {
    //int numberCammander = random.nextInt(10) + 10;
    public OrcCommander() {
        super("Commander", 150, 30, 20, 5, Status.Alive);
    }
}

abstract class TRIBE {
    Random random = new Random();
    int number = 10;

    abstract void gifBonus();
}

class ARMY extends TRIBE {
    Orc[] ARMY = new Orc[number];
    int x = random.nextInt(6) + 10;

    @Override
    void gifBonus() {
        for (int i = 0; i < ARMY.length; i++) {
            ARMY[i].HP = ARMY[i].HP + (x * (ARMY[i].HP * 0.01));
            ARMY[i].DAMAG = ARMY[i].DAMAG + (x * (ARMY[i].DAMAG * 0.01));
            ARMY[i].DEF = ARMY[i].DEF + (x * (ARMY[i].DEF * 0.01));
            ARMY[i].EDA = ARMY[i].EDA + (x * (ARMY[i].EDA * 0.01));

        }
    }
}

class FARM extends TRIBE {
    Orc[] FARM = new Orc[number];
    int x = random.nextInt(3) + 3;
    int y = 20;

    @Override
    void gifBonus() {
        for (int i = 0; i < FARM.length; i++) {
            FARM[i].HP = FARM[i].HP + (x * (FARM[i].HP * 0.01));
            FARM[i].DAMAG = FARM[i].DAMAG + (x * (FARM[i].DAMAG * 0.01));
            FARM[i].DEF = FARM[i].DEF + (x * (FARM[i].DEF * 0.01));
            FARM[i].EDA = FARM[i].EDA + (y * (FARM[i].EDA * 0.01));

        }
    }
}

class CASTLE extends TRIBE {
    Orc[] CASTLE = new Orc[number];
    int x = random.nextInt(6) + 15;

    void test(){
        Random random = new Random();

        Status[] statuses = Status.values();
        System.out.println(
                statuses[random.nextInt(statuses.length)]
        );

    }



    @Override
    void gifBonus() {
        for (int i = 0; i < CASTLE.length; i++) {
            CASTLE[i].HP = CASTLE[i].HP + (x * (CASTLE[i].HP * 0.01));
            CASTLE[i].DAMAG = CASTLE[i].DAMAG + (x * (CASTLE[i].DAMAG * 0.01));
            CASTLE[i].DEF = CASTLE[i].DEF + (x * (CASTLE[i].DEF * 0.01));
            CASTLE[i].EDA = CASTLE[i].EDA + (x * (CASTLE[i].EDA * 0.01));

        }
    }
}

enum Status {
    Alive,
    Dead;
}