package ZadachiBezNomera;

import java.util.Scanner;

public class ZadachaNaKanikuli9 {
    public static void main(String[] args) {
        Time time = new Time();
        time.ystanovka(time.chas, time.min, time.sec);

    }
}

class Time {
    int chas;
    int min;
    int sec;

    public static void ystanovka(int chas, int min, int sec) {
        Scanner scanner = new Scanner(System.in);
        chas = scanner.nextInt();
        min = scanner.nextInt();
        sec = scanner.nextInt();
        min += sec / 60;
        sec = sec % 60;
        chas += min / 60;
        min = min % 60;
        chas = chas % 24;
        System.out.println((chas) + " " + (min) + " " + (sec));
        chas += scanner.nextInt();
        min += scanner.nextInt();
        sec += scanner.nextInt();
        min += sec / 60;
        sec = sec % 60;
        chas += min / 60;
        min = min % 60;
        chas = chas % 24;
        System.out.println((chas) + " " + (min) + " " + (sec));

    }

}