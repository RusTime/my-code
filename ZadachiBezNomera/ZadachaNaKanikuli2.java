package ZadachiBezNomera;

public class ZadachaNaKanikuli2 {
    public static void main(String[] args) {
        int[] data = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i]+" ");
        }
        System.out.println();
        for (int i = 0; i < data.length - 1; i++) {
            int tmp = data[i];
            data[i] = data[i + 1];
            data[i + 1] = tmp;
        }
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i]+" ");
        }
    }
}
