package ZadachiBezNomera;

import java.util.Random;

public class ZadachaNaKanikuli7 {
    public static void main(String[] args) {
        DvaZnacheniya dvaZnacheniya = new DvaZnacheniya();
        dvaZnacheniya.zamena(dvaZnacheniya.x, dvaZnacheniya.y);
        dvaZnacheniya.raschot(dvaZnacheniya.x, dvaZnacheniya.y);
    }
}

class DvaZnacheniya {
    Random random = new Random();
    int x = random.nextInt(50);
    int y = random.nextInt(50);

    public static void zamena(int x, int y) {
        System.out.println(x);
        System.out.println(y);
        Random random = new Random();
        x = random.nextInt(50);
        y = random.nextInt(50);
        System.out.println(x);
        System.out.println(y);
    }

    public static void raschot(int x, int y) {
        System.out.println(x + y);
        if (x > y) {
            System.out.println(x);
        } else {
            System.out.println(y);
        }
    }

}