package ZadachiBezNomera;

import java.util.Random;
import java.util.Scanner;

public class ZadachaNaKanikuli1 {
    public static void main(String[] args) {
        Random random = new Random();
        int numer = random.nextInt(50);
        System.out.println(numer);
        int c = 3;


        proces(numer, c);
    }

    public static void proces(int numer, int c) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        c--;
        if (x == numer) {
            System.out.println("Вы угадали.");
        }else if (x < numer){
            System.out.println("Загаданное число больше.");
        }else if (x>numer){
            System.out.println("Загаданное число меньше.");
        }
        if (c == 0){
            System.out.println("Попытки закончились");
        }else if (c > 0){
            System.out.println("У вас осталось "+ c +" попыток.");
            proces(numer,c);
        }
    }
}
