package ZadachiBezNomera;

public class ZadachaNaKanikuli10 {
    public static void main(String[] args) {
        Abonent[] abonent = new Abonent[5];
        abonent[0] = new Abonent(340120300, "Иванов1", "Иван1", "Ивановичь1", "Москва", 401233, Karta.debet, 6, 0);
        abonent[1] = new Abonent(121415164, "Иванов2", "Иван2", "Ивановичь2", "Москва", 784345, Karta.kredit, 8, 7);
        abonent[2] = new Abonent(311654321, "Иванов3", "Иван3", "Ивановичь3", "Москва", 525754, Karta.debet, 10, 0);
        abonent[3] = new Abonent(472213454, "Иванов4", "Иван4", "Ивановичь4", "Москва", 175424, Karta.debet, 9, 1);
        abonent[4] = new Abonent(136553276, "Иванов5", "Иван5", "Ивановичь5", "Москва", 135457, Karta.kredit, 20, 30);


        pint(abonent);


    }

    public static void pint(Abonent[] abonent) {
        for (int i = 0; i < abonent.length; i++) {
            if (abonent[i].vremyaGorodskie > 0) {
                abonent[i].printAbonent();
            }else
            if (abonent[i].vremyaMejdynarod > 10) {
                abonent[i].printAbonent();
            }

        }
    }
}

class Abonent {
    int IN;
    String Familiya;
    String Imya;
    String Ochistvo;
    String Adres;
    int NumerKarti;
    Karta karta;
    int vremyaMejdynarod;
    int vremyaGorodskie;

    public Abonent(int IN, String familiya, String imya, String ochistvo, String adres, int numerKarti, Karta karta, int vremyaMejdynarod, int vremyaGorodskie) {
        this.IN = IN;
        Familiya = familiya;
        Imya = imya;
        Ochistvo = ochistvo;
        Adres = adres;
        NumerKarti = numerKarti;
        this.karta = karta;
        this.vremyaMejdynarod = vremyaMejdynarod;
        this.vremyaGorodskie = vremyaGorodskie;
    }

    public void printAbonent() {
        System.out.println("Идентификационный номер : " + IN + " | Фамилия : " + Familiya + " | Имя : " + Imya + " | Отчество : " + Ochistvo + " | Адрес : " + Adres + " | Номер кредитной карточки : " + NumerKarti + " | Тип карты : " + karta + " | Время междугородных переговоров : " + vremyaMejdynarod + " | Время городских переговоров : " + vremyaGorodskie + " |");
    }


}

enum Karta {
    debet,
    kredit;
}