package Leson20;

import java.util.Scanner;

public class Zadacha1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a1 = 0;
        int a2 = 0;

        System.out.print("Введите два числа: ");
        a1 = scanner.nextByte();
        a2 = scanner.nextByte();
        System.out.println();
        if (a1 > a2) {
            int tmp = a2;
            a2 = a1;
            a1 = tmp;
        }

        System.out.println("Пифагоровы тройки чисел от " + a1 + " до " + a2 + " включительно:");
        for (int i = a1; i <= a2; i++) {
            for (int j = a1; j <= a2; j++) {
                for (int k = a1; k <= a2; k++) {
                    if (i * i + j * j == k * k)
                        System.out.println("{ " + i + ";" + j + ";" + k + "}");
                }
            }
        }
    }
}
