package Leson16;

import java.util.Random;
import java.util.Scanner;

public class Zadacha4 {
    public static void main(String[] args) {
        play();
    }

    static void play() {
        Scanner scanner = new Scanner(System.in);
        Human Human = new Human();
        Computer Computer = new Computer();
        System.out.println("Привет. Напиши 'newstart' чтобы начать.");
        try {
            activePlayr active = activePlayr.valueOf(scanner.next());
            if (active != activePlayr.newstart) throw new Exception("Pishi pravel'no ****!!");
            while (active != activePlayr.end) {
                if (active == activePlayr.newstart) {
                    Human.getName();
                    Computer.getName();
                    Human.getMove();
                    Computer.getMove();
                    if (Human.reshenie1 == SSP.Stone && Computer.reshenie2 == SSP.Stone) {
                        System.out.println("--Ничья--");
                    }
                    if (Human.reshenie1 == SSP.Stone && Computer.reshenie2 == SSP.Scissors) {
                        System.out.println("Победил " + Human.Name + " !");
                        Human.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Stone && Computer.reshenie2 == SSP.Paper) {
                        System.out.println("Победил " + Computer.computer + " !");
                        Computer.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Scissors && Computer.reshenie2 == SSP.Stone) {
                        System.out.println("Победил " + Computer.computer + " !");
                        Computer.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Scissors && Computer.reshenie2 == SSP.Scissors) {
                        System.out.println("--Ничья--");
                    }
                    if (Human.reshenie1 == SSP.Scissors && Computer.reshenie2 == SSP.Paper) {
                        System.out.println("Победил " + Human.Name + " !");
                        Human.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Paper && Computer.reshenie2 == SSP.Stone) {
                        System.out.println("Победил " + Human.Name + " !");
                        Human.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Paper && Computer.reshenie2 == SSP.Scissors) {
                        System.out.println("Победил " + Computer.computer + " !");
                        Computer.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Paper && Computer.reshenie2 == SSP.Paper) {
                        System.out.println("--Ничья--");
                    }
                    System.out.println(" | У игрока " + Human.Name + " : " + Human.player1 + " очков. " +
                            "| У игрока  " + Computer.computer + " : " + Computer.player2 + " очков. | ");

                }
                if (active == activePlayr.again) {
                    Human.getMove();
                    Computer.getMove();
                    if (Human.reshenie1 == SSP.Stone && Computer.reshenie2 == SSP.Stone) {
                        System.out.println("--Ничья--");
                    }
                    if (Human.reshenie1 == SSP.Stone && Computer.reshenie2 == SSP.Scissors) {
                        System.out.println("Победил " + Human.Name + " !");
                        Human.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Stone && Computer.reshenie2 == SSP.Paper) {
                        System.out.println("Победил " + Computer.computer + " !");
                        Computer.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Scissors && Computer.reshenie2 == SSP.Stone) {
                        System.out.println("Победил " + Computer.computer + " !");
                        Computer.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Scissors && Computer.reshenie2 == SSP.Scissors) {
                        System.out.println("--Ничья--");
                    }
                    if (Human.reshenie1 == SSP.Scissors && Computer.reshenie2 == SSP.Paper) {
                        System.out.println("Победил " + Human.Name + " !");
                        Human.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Paper && Computer.reshenie2 == SSP.Stone) {
                        System.out.println("Победил " + Human.Name + " !");
                        Human.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Paper && Computer.reshenie2 == SSP.Scissors) {
                        System.out.println("Победил " + Computer.computer + " !");
                        Computer.addPoint();
                    }
                    if (Human.reshenie1 == SSP.Paper && Computer.reshenie2 == SSP.Paper) {
                        System.out.println("--Ничья--");
                    }
                    System.out.println(" | У игрока " + Human.Name + " : " + Human.player1 + " очков. " +
                            "| У игрока  " + Computer.computer + " : " + Computer.player2 + " очков. | ");
                }
                System.out.print("Напишите команду('newstart'(что-бы начать с начала),'again'(что-бы сыграть ещё раз),'end'(что-бы выйти)): ");
                active = activePlayr.valueOf(scanner.next());
                if (active != activePlayr.again || active != activePlayr.end || active != activePlayr.newstart) throw new Exception("Pishi pravel'no ****!!('newstart'(что-бы начать с начала),'again'(что-бы сыграть ещё раз),'end'(что-бы выйти))");
                System.out.println();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            play();
        }
    }

}

interface Player {
    String getName();

    SSP getMove();

    int addPoint();

    void getPoints();
}

class Human implements Player {
    int player1 = 0;
    String Name = null;
    SSP reshenie1 = null;

    @Override
    public String getName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Напиши своё имя ==> ");
        Name = scanner.nextLine();
        System.out.println("Первый игрок : " + Name);
        return null;
    }

    @Override
    public SSP getMove() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Напиши 0 это 'Stone' , 1 это 'Scissors' или 2 это 'Paper' .");
//        reshenie1 = SSP.valueOf(scanner.nextLine());
        int numer = scanner.nextInt();
        if (numer == 0) return reshenie1 = SSP.Stone;
        if (numer == 1) return reshenie1 = SSP.Scissors;
        if (numer == 2) return reshenie1 = SSP.Paper;
        return reshenie1;
    }

    @Override
    public int addPoint() {
        player1 += 1;
        return player1;
    }

    @Override
    public void getPoints() {
        System.out.println("Очки игрока " + Name + " = " + player1);
    }
}

class Computer implements Player {
    String computer;
    int player2 = 0;
    SSP reshenie2 = null;

    @Override
    public String getName() {
        Generator generator = new Generator();
        String name = generator.Names();
        computer = name;
        System.out.println("Второй игрок : " + computer);
        return computer;
    }

    @Override
    public SSP getMove() {
        Random random = new Random();
        int numer = random.nextInt(3);
        if (numer == 0) reshenie2 = SSP.Stone;
        if (numer == 1) reshenie2 = SSP.Scissors;
        if (numer == 2) reshenie2 = SSP.Paper;
        System.out.println(reshenie2);
        return reshenie2;
    }

    @Override
    public int addPoint() {
        player2 += 1;
        return player2;
    }

    @Override
    public void getPoints() {
        System.out.println("Очки игрока " + computer + " = " + player2);
    }
}

class Generator {
    String[] partsOfName = {"na", "ma", "no", "bo", "hi", "lu", "o", "a", "pe", "i", "y", "hu", "y", "ch", "ka", "sh"};

    public String Names() {
        Random random = new Random();
        String res = "";
        res += (char) (random.nextInt(27) + 65);
        for (int i = 0; i < random.nextInt(5) + 1; i++) {
            res += partsOfName[random.nextInt(partsOfName.length)];

        }
        return res;
    }
}

enum SSP {
    Stone,
    Scissors,
    Paper;
}

enum activePlayr {
    newstart,
    again,
    end;
}
