package Leson15;

import java.util.Scanner;

public class Zadacha10 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println(numerX(scanner.nextInt()));
    }

    public static int numerX(int numer) {
        int x = 0;
        if (numer != 0) {
            x = numerX(scanner.nextInt());
            if (numer > x) {
                return numer;
            } else return x;
        } else
            return x;
    }
}
