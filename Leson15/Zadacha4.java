package Leson15;

public class Zadacha4 {
    public static void main(String[] args) {
        System.out.println(sum(-1923));
    }

    public static int sum(int n) {
        if (n < 10 && n > -10) {
            return n;
        } else {
            return (n % 10) + sum(n / 10);
        }
    }
}
