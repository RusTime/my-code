package Leson15;

public class Zadacha8 {
    public static void main(String[] args) {
        String string = "HeHfeH";
        func(string);
    }

    public static void func(String str) {
        if (str.charAt(0) == str.charAt(str.length() - 1) && str.length() > 2) {
            func(str.substring(1, str.length() - 1));
        } else if (str.charAt(0) == str.charAt(str.length() - 1) && str.length() <= 2) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
