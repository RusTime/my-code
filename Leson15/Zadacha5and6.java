package Leson15;

public class Zadacha5and6 {
    public static void main(String[] args) {
        symaPL(95887671);
        System.out.println();
        symaLP(-95999971);
    }

    public static void symaPL(int number) {
        if (number < 10 && number > -10) {
            System.out.print(" " + number);
        } else {
            System.out.print(" " + (number % 10));
            symaPL(number / 10);
        }
    }

    public static void symaLP(int number) {
        if (number < 10 && number > -10) {
            System.out.print(" " + number);
        } else {
            symaLP(number / 10);
            System.out.print(" " + (number % 10));
        }
    }
}
