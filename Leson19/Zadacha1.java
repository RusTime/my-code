package Leson19;

import java.util.ArrayList;
import java.util.Random;

public class Zadacha1 {
    public static void main(String[] args) {
        ArrayList<Figyra> data = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int x = random.nextInt(4);
            if (x == 0) {
                data.add(i,new Treygolnik());
            } else if (x == 1) {
                data.add(i,new Kvodrat());
            } else if (x == 2) {
                data.add(i,new  Pryamoygolnik());
            } else if (x == 3) {
                data.add(i,new  TObrazniy());
            } else if (x == 4) {
                data.add(i,new  GObrazniy());
            }
        }
        for (int i = 0; i < data.size(); i++) {
            data.get(i).draw();
            System.out.println();
        }

    }
}

abstract class Figyra {

    abstract void draw();
}

class Treygolnik extends Figyra {


    @Override
    void draw() {
        System.out.println("# ");
        System.out.println("# # ");
        System.out.println("# # # ");
        System.out.println("# # # # ");
        System.out.println("# # # # #");
    }
}

class Kvodrat extends Figyra {


    @Override
    void draw() {
        System.out.println("# # # # #");
        System.out.println("# # # # #");
        System.out.println("# # # # #");
        System.out.println("# # # # #");
        System.out.println("# # # # #");
    }
}

class Pryamoygolnik extends Figyra {
    @Override
    void draw() {
        System.out.println("# # # # # #");
        System.out.println("# # # # # #");
        System.out.println("# # # # # #");
    }
}

class TObrazniy extends Figyra {
    @Override
    void draw() {
        System.out.println("# # # # # #");
        System.out.println("# # # # # #");
        System.out.println("    # #    ");
        System.out.println("    # #    ");
    }
}

class GObrazniy extends Figyra {
    @Override
    void draw() {
        System.out.println("# # # # #");
        System.out.println("# # # # #");
        System.out.println("      # #");
        System.out.println("      # #");
    }
}